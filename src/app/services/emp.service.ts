import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserData } from '../model/emp/userData';

@Injectable({
  providedIn: 'root'
})
export class EmpService {
  url: string = environment.base_url;
  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get(this.url+"/users");
  }

  getByOne(id: string) {
    return this.http.get(this.url+"/users"+id);
  }

  create(data: UserData) {
    return this.http.post(this.url+"/users/new", data);
  }

  update(id: string, data: Object) {
    return this.http.put(this.url+"/users/"+id, data);
  }

  delete(id: string) {
    return this.http.delete(this.url+"/users/"+id);
  }
}
