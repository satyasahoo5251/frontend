import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EmpService } from '../services/emp.service';

@Component({
  selector: 'app-emp-dialog',
  templateUrl: './emp-dialog.component.html',
  styleUrls: ['./emp-dialog.component.scss']
})
export class EmpDialogComponent implements OnInit {
  first_name:string = "";
  last_name:string = "";
  email:string = "";
  gender:string = "";
  salary:number = 0;
  updateValue: boolean = false;

  constructor( public dialogRef: MatDialogRef<EmpDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private service: EmpService) { }

  ngOnInit(): void {
    this.first_name = this.data.first_name;
    this.last_name = this.data.last_name;
    this.email = this.data.email;
    this.gender = this.data.gender;
    this.salary = this.data.salary;
  }

  async update() {
    let newData: Object = {
      first_name : this.first_name,
      last_name : this.last_name,
      email : this.email,
      gender : this.gender,
      salary : this.salary,
    }
    return this.service.update(this.data._id, newData).subscribe(
      (e: any) => {
        // console.log(e);
        this.updateValue = true;
        this.onNoClick(this.updateValue)
      }, (error: any) => {
        console.log(error);
      }
    )
  }
  onNoClick(updateValue: boolean): void {
    this.dialogRef.close(updateValue);
  }
}
