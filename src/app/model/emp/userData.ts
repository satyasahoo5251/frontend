export interface UserData {
    id: String;
    first_name: String;
    last_name: String;
    gender: String;
    email: String;
    salary: Number;
}
  