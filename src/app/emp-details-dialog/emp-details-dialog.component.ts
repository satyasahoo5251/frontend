import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EmpService } from '../services/emp.service';

@Component({
  selector: 'app-emp-details-dialog',
  templateUrl: './emp-details-dialog.component.html',
  styleUrls: ['./emp-details-dialog.component.scss']
})
export class EmpDetailsDialogComponent implements OnInit {
  first_name:string = "";
  last_name:string = "";
  email:string = "";
  gender:string = "";
  salary:number = 0;
  updateValue: boolean = true;

  constructor( public dialogRef: MatDialogRef<EmpDetailsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private service: EmpService) { }

  ngOnInit(): void {
    this.first_name = this.data.first_name;
    this.last_name = this.data.last_name;
    this.email = this.data.email;
    this.gender = this.data.gender;
    this.salary = this.data.salary;
  }

  close(updateValue: boolean): void {
    this.dialogRef.close(updateValue);
  }
}
