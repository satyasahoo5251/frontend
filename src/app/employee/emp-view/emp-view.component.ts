import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core'
import { MatDialog } from '@angular/material/dialog'
import { MatPaginator } from '@angular/material/paginator'
import { MatSort } from '@angular/material/sort'
import { MatTableDataSource } from '@angular/material/table'
import { Router } from '@angular/router'
import { EmpDetailsDialogComponent } from 'src/app/emp-details-dialog/emp-details-dialog.component'
import { EmpDialogComponent } from 'src/app/emp-dialog/emp-dialog.component'
import { UserData } from '../../model/emp/userData'
import { EmpService } from '../../services/emp.service'

@Component({
  selector: 'app-emp-view',
  templateUrl: './emp-view.component.html',
  styleUrls: ['./emp-view.component.scss'],
})
export class EmpViewComponent implements OnInit, AfterViewInit {
  
  displayedColumns: string[] = [
    'first_name',
    'last_name',
    'gender',
    'email',
    'salary',
    'action',
  ]
  dataSource!: MatTableDataSource<UserData>
  users!: UserData[]
  @ViewChild(MatPaginator) paginator!: MatPaginator
  @ViewChild(MatSort) sort!: MatSort

  constructor(private service: EmpService, public dialog: MatDialog, private route: Router) {}
  ngOnInit(): void {
    this.getData();
  }

  ngAfterViewInit() {}

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value
    this.dataSource.filter = filterValue.trim().toLowerCase()

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage()
    }
  }

  async getData() {
    return (await this.service.getAll()).subscribe(
      (e: any) => {
        this.users = e.reverse();
        this.dataSource = new MatTableDataSource(this.users)
        this.dataSource.paginator = this.paginator
        this.dataSource.sort = this.sort
      },
      (error) => {
        console.log(error)
      },
    )
  }

  add() {
    this.route.navigate(["/add"])
  }
  edit(e: any): void {
    const dialogRef = this.dialog.open(EmpDialogComponent, {
      width: '50%',
      data: e,
    });
    // console.log((e));
    dialogRef.afterClosed().subscribe(result => {
      if(result === true) {
        this.getData();
      }
    });
  }
  async delete(e: any) {
    let text = 'Do you want to confirm to delete ' + e._id;
    if (confirm(text) === true) {
      this.service.delete(e._id).subscribe(
        (e: any) => {
          this.getData()
        },
        (error: any) => {
          console.log(error)
        },
      )
    } else {
      console.log("Can't Deleted")
    }
  }
  async openDialog(e: any) {
    const dialogRef = this.dialog.open(EmpDetailsDialogComponent, {
      width: '30%',
      data: e,
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result === true) {
        this.getData();
      }
    });
  }
}
