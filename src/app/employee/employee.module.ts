import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeComponent } from './employee.component';
import { EmployeeRoutingModule } from './employee-routing.module';
import {MatTableModule} from '@angular/material/table';
import { EmpViewComponent } from './emp-view/emp-view.component';
import { EmpAddComponent } from './emp-add/emp-add.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import { EmpDialogComponent } from '../emp-dialog/emp-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { EmpDetailsDialogComponent } from '../emp-details-dialog/emp-details-dialog.component';
import {MatRadioModule} from '@angular/material/radio';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatExpansionModule} from '@angular/material/expansion';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [
    EmployeeComponent,
    EmpViewComponent,
    EmpAddComponent,
    EmpDialogComponent,
    EmpDetailsDialogComponent,
  ],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatCardModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatRadioModule,
    FlexLayoutModule,
    MatExpansionModule,
    MatGridListModule,
  ],
  exports: [
    EmployeeComponent,
    EmpViewComponent,
    EmpAddComponent,
    EmpDialogComponent,
    EmpDetailsDialogComponent,
  ]
})
export class EmployeeModule { }
