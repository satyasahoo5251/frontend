import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EmpService } from 'src/app/services/emp.service';

@Component({
  selector: 'app-emp-add',
  templateUrl: './emp-add.component.html',
  styleUrls: ['./emp-add.component.scss']
})
export class EmpAddComponent implements OnInit {
  empForm!: FormGroup;
  constructor(private fb: FormBuilder, private sevice: EmpService, private route: Router) {
    this.empForm = this.fb.group({
      first_name: ["", [Validators.required]],
      last_name: ["", Validators.required],
      email: ["", [Validators.required, Validators.email]],
      gender: ["", Validators.required],
      salary: [0, Validators.required],
    })
  }

  ngOnInit(): void {
  }

  create() {
    return this.sevice.create(this.empForm.value).subscribe(
      (res) => {
        console.log(res);
        this.route.navigate([""]);
      }, (error) => {
        console.log(error);
        alert(error.error.message);
      }
    )
  }
  get email() {
    return this.empForm.get('email');
  } 
}
