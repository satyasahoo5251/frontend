import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmpAddComponent } from './emp-add/emp-add.component';
import { EmpViewComponent } from './emp-view/emp-view.component';

const routes: Routes = [
  {
    path: "",
    component: EmpViewComponent,
    pathMatch: "full"
  },
  {
    path: "add",
    component: EmpAddComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
